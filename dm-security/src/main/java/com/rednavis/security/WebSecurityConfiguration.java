package com.rednavis.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.web.cors.reactive.CorsConfigurationSource;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.function.Function;

import static java.lang.String.format;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpMethod.OPTIONS;
import static org.springframework.web.reactive.function.client.ExchangeFilterFunction.ofRequestProcessor;
import static reactor.core.publisher.Mono.just;


@Configuration
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
public interface WebSecurityConfiguration {

    @Bean
    default SecurityWebFilterChain securityWebFilterChain(final ServerHttpSecurity http){
        return http
                .cors()
                .and()
                .authorizeExchange()
                .pathMatchers(OPTIONS)
                .permitAll()
                .anyExchange()
                .authenticated()
                .and()
                .oauth2ResourceServer()
                .jwt()
                .and()
                .and()
                .build();
    }
}

