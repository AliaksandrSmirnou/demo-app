package com.rednavis.mongo;

import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.gridfs.GridFSBucket;
import com.mongodb.reactivestreams.client.gridfs.GridFSBuckets;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

//@Configuration
@EnableReactiveMongoRepositories
//@PropertySource(value = "classpath:dm-db.yml", factory = YamlPropertyLoaderFactory.class)
public class MongoConfiguration {

    @Bean
    public GridFSBucket gridFSBucket(MongoClient mongoClient,
                                      @Value("demo-buckets") String bucketsName){
        return GridFSBuckets.create(mongoClient.getDatabase(bucketsName));
    }
}

