package com.rednavis.api.rest;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import reactor.core.publisher.Flux;

@RequestMapping("/tasks")
public interface TaskApi<T> {

    @GetMapping("/{type}")
    Flux<T> all(@PathVariable String type);

    @GetMapping("/{type}/search")
    Flux<T> find(@PathVariable String type, @RequestParam String query);

}
