package com.rednavis.api.rest;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@RequestMapping("/users")
public interface UserApi<U> {

    @GetMapping
    Flux<U> all();

    @GetMapping("/{id}")
    Mono<U> one(@PathVariable String id);

    @DeleteMapping("/{id}")
    Mono<Void> delete(@PathVariable String id);

    @PutMapping
    Mono<U> update(@RequestBody U user);

    @PostMapping
    Mono<U> create(@RequestBody U user);

}
