package com.rednavis.api.rest;

import com.rednavis.domain.User;
import com.rednavis.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
public class UserController
        implements UserApi<User> {

    private final UserRepository userRepository;

    @Override
    public Flux<User> all() {
        return userRepository.findAll();
    }

    @Override
    public Mono<User> one(String id) {
        return userRepository.findById(id);
    }

    @Override
    public Mono<Void> delete(String id) {
        return userRepository.deleteById(id);
    }

    @Override
    public Mono<User> update(User user) {
        return userRepository.save(user);
    }

    @Override
    public Mono<User> create(User user) {
        return userRepository.save(user);
    }
}
