package com.rednavis.api.rest;

import com.rednavis.domain.Task;
import com.rednavis.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@RequiredArgsConstructor
public class TaskController
        implements TaskApi<Task> {

    private final TaskRepository taskRepository;

    @Override
    public Flux<Task> all(String type) {
        return taskRepository.findAll();
    }

    @Override
    public Flux<Task> find(String type, String query) {
        return null;
    }
}
