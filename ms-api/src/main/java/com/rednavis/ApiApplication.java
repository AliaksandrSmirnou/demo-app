package com.rednavis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public interface ApiApplication {
    static void main(String[] args) {
        SpringApplication.run(ApiApplication.class, args);
    }
}
