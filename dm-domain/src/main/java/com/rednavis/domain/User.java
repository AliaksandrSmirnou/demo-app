package com.rednavis.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.Value;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Value
@Builder
@Document
@RequiredArgsConstructor(onConstructor_ = {@PersistenceConstructor, @JsonCreator})
public class User {

    String id;
    String name;


    public User withId(String id){
        return User.builder().id(id).build();
    }
}
