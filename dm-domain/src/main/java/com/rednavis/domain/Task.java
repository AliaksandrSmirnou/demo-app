package com.rednavis.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Value
@Builder
@Document
@RequiredArgsConstructor(onConstructor_ = {@PersistenceConstructor, @JsonCreator})
public class Task {

    private String id;
    private String name;

    public Task withId(String id){
        return Task.builder().id(id).build();
    }

}
