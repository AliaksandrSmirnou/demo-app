package com.rednavis.discovery;


import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
public interface DiscoveryConfiguration {
}
