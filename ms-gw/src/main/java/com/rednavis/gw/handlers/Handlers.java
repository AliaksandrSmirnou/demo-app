package com.rednavis.gw.handlers;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.security.Principal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;

import static org.springframework.web.reactive.function.server.ServerResponse.ok;
import static org.springframework.web.reactive.function.server.ServerResponse.permanentRedirect;
import static org.springframework.web.util.UriComponentsBuilder.fromUri;
import static reactor.core.publisher.Mono.just;

@Component
public class Handlers {

    @Value("${spring.application.name}")
    private String appName;
    @Value("${server.port}")
    private int port;


    public final Mono<ServerResponse> fallback(final ServerRequest request) {
        return ok().body(just("Hystrix fallback."), String.class);
    }

    public final Mono<ServerResponse> echo(final ServerRequest request) {
        return ok().body(request.bodyToMono(String.class).switchIfEmpty(Mono.just("echo")), String.class);
    }

    public final Mono<ServerResponse> welcome(final ServerRequest request) {
        final UriComponents uri = UriComponentsBuilder.fromUri(request.uri())
                .port(port)
                .path("actuator/gateway/routes")
                .build();
        final Map<String, String> infoMap = new LinkedHashMap<>();
        infoMap.put("message", "U Are Welcome to Gateway App!");
        infoMap.put("app.name", appName);
        infoMap.put("app.routes", uri.toString());
        final Function<String, Map> addUsername = name -> {
            infoMap.put("app.user", name);
            return infoMap;
        };
        return ok().body(request
                .principal()
                .map(Principal::getName)
                .map(addUsername)
                .switchIfEmpty(Mono.just(infoMap)), Map.class);
    }

//    @SneakyThrows
//    public final Mono<ServerResponse> routesRedirect(final ServerRequest request) {
//        UriComponents uri = fromUri(request.uri()).path("actuator/gateway/routes").build();
//        return permanentRedirect(uri.toUri()).build();
//    }


}
