package com.rednavis.gw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public interface GwApplication {

	static void main(String[] args) {
		SpringApplication.run(GwApplication.class, args);
	}

//	@Bean
//	public DiscoveryClientRouteDefinitionLocator discoveryClientRouteLocator(DiscoveryClient discoveryClient,
//																			  DiscoveryLocatorProperties discoveryLocatorProperties) {
//		return new DiscoveryClientRouteDefinitionLocator(discoveryClient, discoveryLocatorProperties);
//	}

}

